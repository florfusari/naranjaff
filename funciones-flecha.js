let sumarNumeros = (a,b) => {
    return a + b;
}

let restarNumeros = (a,b) => {
    return a - b;
}

let dividirNumeros = (a,b) => {
    if (b === 0) {
        return 'No es posible dividir por cero'
    } else {
        return a / b;
    }
    
}

let multiplicarNumeros = (a,b) => {
    return a * b;
}

console.log('Suma: ' + sumarNumeros(1,2) + '\nResta: ' + restarNumeros(1,2) + '\nDivisión: ' + dividirNumeros(1,2) + '\nMultiplicación: ' + multiplicarNumeros(1,2));
